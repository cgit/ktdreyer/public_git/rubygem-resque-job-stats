%global gem_name resque-job-stats

Name: rubygem-%{gem_name}
Version: 0.3.0
Release: 3%{?dist}
Summary: Job-centric stats for Resque
Group: Development/Languages
License: MIT
URL: https://github.com/alanpeabody/resque-job-stats
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1: %{name}-test.conf
%if 0%{?fc19} || 0%{?fc20} || 0%{?el7}
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(resque) => 1.17
Requires: rubygem(resque) < 2
%endif
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(resque) => 1.17
BuildRequires: rubygem(resque) < 2
BuildRequires: rubygem(rack-test)
BuildRequires: rubygem(timecop)
BuildRequires: redis
BuildArch: noarch
%if 0%{?fc19} || 0%{?fc20} || 0%{?el7}
Provides: rubygem(%{gem_name}) = %{version}
%endif

%description
Tracks jobs performed, failed, and the duration of the last 100 jobs for each
job type.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove developer-only files.
for f in .document .travis.yml .rvmrc Gemfile Rakefile VERSION; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done

# Remove dependency on bundler.
sed -i -e "/require 'bundler'/d" test/helper.rb
sed -i -e "/Bundler.setup/d" test/helper.rb

# Remove dependency on redgreen.
sed -i "/require 'redgreen'/d" test/helper.rb

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# Remove unnecessary gemspec file
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  # Redis server configuration
  install -m 0644 %{SOURCE1} test/test.conf
  mkdir test/db

  # Start redis-server
  redis-server test/test.conf

  # Run resque tests
  ruby -I"lib:test" -e 'Dir.glob "./test/**/test_*.rb", &method(:require)'

  # Kill redis-server
  kill -INT `cat test/db/redis.pid`
popd


%files
%dir %{gem_instdir}
%doc %{gem_instdir}/LICENSE.txt
%doc %{gem_instdir}/README.rdoc
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%exclude %{gem_instdir}/test

%changelog
* Sun Jul 13 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.0-3
- Avoid using the full path to redis-server during %%check, since this has
  changed in Fedora 21
- Fix test suite libs include dir

* Tue May 27 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.0-2
- Adjustments for https://fedoraproject.org/wiki/Changes/Ruby_2.1
- Drop patch0 in favor of a more future-proof Bundler-removal solution

* Mon Dec 02 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.0-1
- Initial package
